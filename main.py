from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from expression_tree import exp_tree, ExpressionTree

kv ="""
<Calculator>:
    exp:exp
    result:result
    tree:tree
    orientation: 'vertical'
    padding: dp(12), dp(12), dp(12), dp(12)
    spacing: dp(12)
    canvas:
        Color:
            rgba: 9/255, 132/255, 227/255 ,1
            # rgba: 1, 234/255, 167/255 ,1
        Rectangle:
            size: self.size
            pos: self.pos
    BoxLayout:
        orientation: 'vertical'
        height: dp(50)
        size_hint_y: None
        GridLayout:
            cols: 3
            spacing: dp(10)
            GridLayout:
                cols: 1
                size_hint_x: 0.2
                canvas:
                    Color:
                        rgba: 0.5, 0.5, 0.5 ,1
                    Rectangle:
                        size: self.size
                        pos: self.pos
                Label:
                    
                    text: 'Expression :'
                    color: 1,1,1,1
                    bold: True
                    font_size: '22sp'

            TextInput:
                id: exp
                size_hint_x: 0.5
                hint_text: 'Enter Expression'
                font_size: '20sp' 
                padding: dp(12), dp(14), 0, 0
            Button:
                size_hint_x: 0.15
                text: 'Calculate'
                bold: True
                font_size: '22sp'
                on_press: root.on_click_calculate()

    BoxLayout:
        orientation: 'vertical'
        canvas:
            Color:
                rgba: 1, 234/255, 167/255 ,1
                # rgba: 9/255, 132/255, 227/255 ,1
            Rectangle:
                size: self.size
                pos: self.pos
        GridLayout:
            id: tree
            cols:1
            padding: 0, dp(14), 0, 0
            Label:
                text:'Binary Tree of Expression'
                color: 0,184/255,148/255,1
                size_hint_y: None
                height: dp(20)
                bold: True
                font_size: '22sp'
            # Image:
            #     id: tree
            #     source: ''
            #     size: self.width, self.height
            # Label:
            #     id: tree
            #     text: ''
            #     color: 121/255, 85/255, 72/255,1.0
            #     bold: True
            #     font_size: '22sp'
            #     halign: 'left'
            #     valign: 'middle'
            #     font_name: "font/CONSOLA.ttf" 
    BoxLayout:
        orientation: 'vertical'
        height: dp(50)
        size_hint_y: None
        GridLayout:
            cols: 2
            spacing: dp(10)
            GridLayout:
                cols: 2
                canvas:
                    Color:
                        rgba: 0.5, 0.5, 0.5 ,1
                    Rectangle:
                        size: self.size
                        pos: self.pos
                Label:
                    text: 'Result :'
                    color: 1,1,1,1
                    bold: True
                    font_size: '22sp'
                Label:
                    id: result
                    text: ''        
                    bold: True
                    font_size: '22sp'
            Button:
                text: 'Clear'
                size_hint_x: 0.2
                bold: True
                font_size: '22sp'
                on_press: root.on_click_clear()

"""

Builder.load_string(kv)

class Calculator(BoxLayout):
    def __init__(self, **kwargs):
        super(Calculator,self).__init__(**kwargs)
    
    def on_click_calculate(self):
        '''
        this function will activate when user click on calculate button
        '''
        self.tree.clear_widgets()
        text = Label(text='Binary Tree of Expression',color=(0,184/255,148/255,1),size_hint_y= None, height='20dp',bold=True,font_size='22sp')
        self.tree.add_widget(text)

        self.result.color = [1,1,1,1]
        error = 'Calculate Error'
        exp = self.exp.text
        result_tree = exp_tree(exp) if exp_tree(exp) else '' 

        evaluate = result_tree.evaluate() if result_tree else ''     #input result into result label
        self.result.text = str(evaluate) if evaluate else error
        # self.tree.text = result_tree.display() if result_tree else ''             #input tree into tree label
        if self.result.text in error :
            self.result.color = [192/255, 57/255, 43/255,1.0]

        # self.tree.source = 'c_tree.png'
        # self.tree.reload()
        if result_tree:
            pic = Image(source='c_tree.png')
            pic.reload()
            self.tree.add_widget(pic)

        
        
    
    def on_click_clear(self):
        '''
        this function will activate when user click on clear button

        this function will clear the expression text input, result label and tree label
        '''
        self.exp.text = ''
        self.result.text = ''

        # self.tree.source = ''
        self.tree.clear_widgets()
        text = Label(text='Binary Tree of Expression',color=(0,184/255,148/255,1),size_hint_y= None, height='20dp',bold=True,font_size='22sp')
        self.tree.add_widget(text)

class MainApp(App):
    def build(self):
        return Calculator()


if __name__ == '__main__':
    MainApp().run()