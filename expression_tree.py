from tree import LinkedBinaryTree
import pydot

class ExpressionTree(LinkedBinaryTree) :

    def __init__(self, parent, left = None, right = None) :

        self._parent = parent
        super().__init__()
        self._add_root(self._parent)
        if left != None : # add parent leftchild rightchild when new object is operator
            self._attach(self.root(), left, right)
    
    def evaluate(self):
        self.pos_node = {}
        self.node_count = 0
        self.graph = pydot.Dot(graph_type='graph')
        self.build_dot_tree(self.root())
        return self._operator_cal(self.root())

    def _operator_cal(self,p):

        if self.is_leaf(p): 
            return float(p.element())

        # evaluate left tree 
        left_sum = self._operator_cal(self.left(p))          
        # evaluate right tree
        right_sum = self._operator_cal(self.right(p))  

        # check operator and calculate
        if p.element() == '+': 
            try:
                return left_sum + right_sum 
            except Exception:
                return False 
        elif p.element()  == '-': 
            try:
                return left_sum - right_sum 
            except Exception:
                return False 
        elif p.element()  == '*': 
            try:
                return left_sum * right_sum 
            except Exception:
                return False 
        elif p.element() == '/':
            try:
                return left_sum / right_sum 
            except Exception:
                return False 
        else:
            try:
                return left_sum ** right_sum 
            except Exception:
                return False 
    
    def display(self):
        lines, _, _, _ = self._display_aux(self.root())
        return "\n".join(lines)

    def _display_aux(self,pos):
        """Returns list of strings, width, height, and horizontal coordinate of the root.
		
		
			this function from : https://stackoverflow.com/questions/34012886/print-binary-tree-level-by-level-in-python?fbclid=IwAR3-IbDG_DLzaEWlUxohuTtCX1ppGoblUmH1nKEw2kYXTDjeKkm6NBCdJu4
		"""

        # Is leaf.
        if self.is_leaf(pos):
            line = '%s' % pos.element()
            width = len(line)
            height = 1
            middle = width // 2
            return [line], width, height, middle

        # Two children.
        left, n, p, x = self._display_aux(self.left(pos))
        right, m, q, y = self._display_aux(self.right(pos))
        s = '[%s]' % pos.element()
        u = len(s)
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        if p < q:
            left += [n * ' '] * (q - p)
        elif q < p:
            right += [m * ' '] * (p - q)
        zipped_lines = zip(left, right)
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        return lines, n + m + u, max(p, q) + 2, n + u // 2

    def build_dot_tree(self,pos) :
        #if self.is_leaf(pos) :
        #    pass
        #else :
        #    self.build_dot_tree(self.left(pos))
        #    edge = pydot.Edge(pos.element(), self.left(pos).element())
        #    self.graph.add_edge(edge)

        #    self.build_dot_tree(self.right(pos))
        #    edge = pydot.Edge(pos.element(), self.right(pos).element())
        #    self.graph.add_edge(edge)

        if self.is_leaf(pos) :
            if str(pos) not in self.pos_node :
                self.node_count += 1
                self.pos_node[str(pos)] = self.node_count
            return pos.element() + '(' + str(self.pos_node[str(pos)]) + ')'
        else :
            if str(pos) not in self.pos_node :
                self.node_count += 1
                self.pos_node[str(pos)] = self.node_count

            edge = pydot.Edge(pos.element() + '(' + str(self.pos_node[str(pos)]) + ')', self.build_dot_tree(self.left(pos)))
            self.graph.add_edge(edge)

            edge = pydot.Edge(pos.element() + '(' + str(self.pos_node[str(pos)]) + ')', self.build_dot_tree(self.right(pos)))
            self.graph.add_edge(edge)

            self.node_count += 1

            self.graph.write_png('c_tree.png')
            return pos.element() + '(' + str(self.pos_node[str(pos)]) + ')'

        

#[END] EXPRESSION TREE CLASS -----------------------------------

def convert_to_list(expression) :
    paren = []
    for c, i in enumerate(expression):
        if i == '(':
            paren.append(i)
        elif i == ')':
            try:
                paren.pop()
            except Exception:
                return False
    if paren:
        return False

    expression += " "
    expression_list = []            #keep expression array
    operator_list = "+-*/()^ "
    cur_num = ""                    #use to keep many digit number and negative number
    for c, i in enumerate(expression) :         #split the string and convert to list loop
        if i in operator_list :
            if i == '-' and expression_list != [] and cur_num == '':
                if expression_list[-1] in operator_list :    #condition will check the negative number
                    cur_num += i
            elif i == '-' and c == 0 :
                cur_num += i
            else :
                if cur_num != "" :
                    expression_list.append(cur_num)         #add current number into the expression_list
                    cur_num = ""
                expression_list.append(i)                   #append operator into the expression_list
        elif i in "0123456789." :               #condition will check many digit number
            cur_num += i 
    return expression_list

def infix_to_postfix(exp):
    priority = {'+':1, '-':1, '*':2, '/':2, '^':3}
    stack = [] # for operator and ( )
    output = [] # for result
    inf_list = convert_to_list(exp)
    if not inf_list:
        return False
    for ch in inf_list:
        if ch != " ":
                
            if ch not in "+-*/()^": # check operand
                output.append(ch) # push operand to output
            elif ch == '(': # check open parenthesis
                stack.append('(') # push ( to stack
            elif ch == ')': # check close parenthesis
                while stack and stack[-1] != '(': # do when stack is not empty and do until last index is ( open parenthesis
                    output.append(stack.pop()) # push last index in stack to output
                stack.pop() # pop open parenthesis
            else:
                """
                for check operator
                it same check close parenthesis and check priority of operator
                if each operator priority in loop less than or equal then push pop stack to output 
                """
                while stack and stack[-1] != '(' and priority[ch] <= priority[stack[-1]]:
                    output.append(stack.pop())
                stack.append(ch) # push operator

    while stack: output += stack.pop() # push operator until stack is empty
    return output

def exp_tree(exp):
    pos_exp_list = infix_to_postfix(exp)         #convert infix expression to postfix expression
    if not pos_exp_list:
        return False
    stack = []                                # keep subtree stack
    for i in pos_exp_list :
        if i in "+-*/^" :
            try:
                operator = i                         #defind the operator as node
                right = stack.pop()                  #pop subtree from the stack and use tp left or right child
                left = stack.pop()
            except Exception:
                return False
            stack.append(ExpressionTree(operator, left, right))         #attach left and right child to operator node and append back to the stack
        elif i not in "+-*/^" :
            stack.append(ExpressionTree(i))     # if i is number, number do not have child
    return stack.pop()      #the last element in the stack is full expression tree


# exp = "12 * ( 2 + ( 3 / 4 ) ) * 76 - 2"
# exp_tree(exp)

# exp2 = "(400 ^ 2 / (2 * 2) + 10)"
# print(calculate(exp2))

# exp3 = "( 3 - ( 2 * 2 ) * ( 3 / 2 ) ) + ( 14 + 1 )"
# print(calculate(exp3))